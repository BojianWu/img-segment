/*
Copyright (C) 2006 Pedro Felzenszwalb

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*/

#ifndef SEGMENT_IMAGE
#define SEGMENT_IMAGE

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <cstdlib>
#include <vector>
#include "segment-graph.h"

cv::RNG rgb(0xFFFFFFFF);

cv::Vec3b random_color()
{
	int color = (unsigned)rgb;
	return cv::Vec3b(color & 255, (color >> 8) & 255, (color >> 16) & 255);
}

double color_diff(cv::Vec3b color1, cv::Vec3b color2)
{
	return sqrt(double((color1[0] - color2[0])*(color1[0] - color2[0])
		+ (color1[1] - color2[1])*(color1[1] - color2[1])
		+ (color1[2] - color2[2])*(color1[2] - color2[2])));
}

/* Segment an image
 *
 * Returns a color image representing the segmentation.
 *
 * im: image to segment.
 * sigma: to smooth the image.
 * c: constant for threshold function.
 * min_size: minimum component size (enforced by post-processing stage).
 * num_ccs: number of connected components in the segmentation.
 */
cv::Mat segment_image(const cv::Mat& img, float sigma, float c, int min_size)
{
	int width = img.cols;
	int height = img.rows;

	cv::Mat imgSmooth(img.size(), img.type());
	cv::GaussianBlur(img, imgSmooth, cv::Size(5, 5), sigma, sigma);

	edge *edges = new edge[width*height*4];
	int num = 0;

	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++)	{
			//						     (y-1, x+1)
			//				           /  
			//	current pixel(y,   x)  - (y,   x+1)
			//				     |	   \
			//			     (y+1, x)    (y+1, x+1)
			//
			if (x < width - 1) {
				edges[num].a = y * width + x;
				edges[num].b = y * width + (x + 1);
				edges[num].w = color_diff(imgSmooth.at<cv::Vec3b>(y, x), imgSmooth.at<cv::Vec3b>(y, x + 1));
				num++;
			}

			if (y < height - 1)	{
				edges[num].a = y * width + x;
				edges[num].b = (y + 1) * width + x;
				edges[num].w = color_diff(imgSmooth.at<cv::Vec3b>(y, x), imgSmooth.at<cv::Vec3b>(y + 1, x));
				num++;
			}

			if ((x < width - 1) && (y < height - 1)) {
				edges[num].a = y * width + x;
				edges[num].b = (y + 1) * width + (x + 1);
				edges[num].w = color_diff(imgSmooth.at<cv::Vec3b>(y, x), imgSmooth.at<cv::Vec3b>(y + 1, x + 1));
				num++;
			}

			if ((x < width - 1) && (y > 0)) {
				edges[num].a = y * width + x;
				edges[num].b = (y - 1) * width + (x + 1);
				edges[num].w = color_diff(imgSmooth.at<cv::Vec3b>(y, x), imgSmooth.at<cv::Vec3b>(y - 1, x + 1));
				num++;
			}
		}
	}

	// segment
	universe *u = segment_graph(width*height, num, edges, c);

	// post process small components
	for (int i = 0; i < num; i++) {
		int a = u->find(edges[i].a);
		int b = u->find(edges[i].b);
		if ((a != b) && ((u->size(a) < min_size) || (u->size(b) < min_size)))
			u->join(a, b);
	}
	delete [] edges;

	cv::Mat output(img.size(), img.type());
	std::vector<cv::Vec3b> colormap(height*width);

	for (int i = 0; i < colormap.size(); ++i)
		colormap[i] = random_color();

	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			int comp = u->find(y * width + x);
			output.at<cv::Vec3b>(y, x) = colormap[comp];
		}
	}

	return output;
}

#endif
