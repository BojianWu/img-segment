Graph-based Image Segmentation
---------------------------------------------------
This is a re-implementation of the source code by using OpenCV 2.4.9. The source code is downloaded from the website as follows. For more information, please refer to the related paper. 

	http://cs.brown.edu/~pff/segment/

NOTICES:

The original algorithm only supports *.ppm *.pgm input image type, the opencv-based implementation can then support more types.
